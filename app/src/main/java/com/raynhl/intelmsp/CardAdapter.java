package com.raynhl.intelmsp;

import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;


/**
 * Created by raynhl on 1/22/17.
 */

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {
    List<ListItem> items;

    public CardAdapter(String[] facilityTypes, int[] facilityIcons, int[] facilitySubIcons,
                       int[] availables, String[] buildingNames, String[] levels,  String[] timeStamps,
                         int[] cardBackgroundColors, String[] availableFacilityTypes, int position) {
        super();

        items = new ArrayList<ListItem>();

        if (position == 0) {
            for (int i = 0; i < buildingNames.length; i++) {
                ListItem item = new ListItem();

                item.setFacilityType(facilityTypes[i]);
                item.setFacilityIcon(facilityIcons[i]);
                item.setFacilitySubIcon(facilitySubIcons[i]);
                item.setAvailable(availables[i]);
                item.setBuildingName(buildingNames[i]);
                item.setLevel(levels[i]);
                item.setTimeStamp(timeStamps[i]);
                item.setCardBackgroundColor(cardBackgroundColors[i]);
                items.add(item);
            }
        }

        else {
            String currentFacilityType = availableFacilityTypes[position];
            for (int i = 0; i < buildingNames.length; i++) {
                ListItem item = new ListItem();

                if (currentFacilityType.equals(facilityTypes[i])) {
                    item.setFacilityType(facilityTypes[i]);
                    item.setFacilityIcon(facilityIcons[i]);
                    item.setFacilitySubIcon(facilitySubIcons[i]);
                    item.setAvailable(availables[i]);
                    item.setBuildingName(buildingNames[i]);
                    item.setLevel(levels[i]);
                    item.setTimeStamp(timeStamps[i]);
                    item.setCardBackgroundColor(cardBackgroundColors[i]);
                    items.add(item);
                }
            }
        }
    }

    @Override
    public CardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater lf = LayoutInflater.from(parent.getContext());
        View v = lf.inflate(R.layout.list_card_view, parent, false);
        CardAdapter.ViewHolder viewHolder = new CardAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ListItem list = items.get(position);
        holder.textViewFacilityType.setText(list.getFacilityType());
        holder.imageViewFacilityIcon.setImageResource(list.getFacilityIcon());
        holder.imageViewFacilitySubIcon.setImageResource(list.getFacilitySubIcon());

        holder.textViewAvailable.setText(Integer.toString(list.getAvailable()));
        holder.textViewBuildingName.setText(list.getBuildingName());
        holder.textViewLevel.setText(list.getLevel());

        holder.textViewTimeStamps.setText(list.getTimeStamp());

        LinearLayout linearLayout = (LinearLayout) holder.itemView;
        CardView cardView = (CardView) linearLayout.getChildAt(0);
        cardView.setCardBackgroundColor(list.getCardBackgroundColor());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewFacilityType;
        public ImageView imageViewFacilityIcon;
        public ImageView imageViewFacilitySubIcon;
        public TextView textViewAvailable;
        public TextView textViewBuildingName;
        public TextView textViewLevel;
        public TextView textViewTimeStamps;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewFacilityType = (TextView) itemView.findViewById(R.id.textViewFacilityType);
            imageViewFacilityIcon = (ImageView) itemView.findViewById(R.id.imageViewFacilityIcon);
            imageViewFacilitySubIcon = (ImageView) itemView.findViewById(R.id.imageViewFacilitySubIcon);
            textViewAvailable = (TextView) itemView.findViewById(R.id.textViewAvailable);
            textViewBuildingName = (TextView) itemView.findViewById(R.id.textViewBuildingName);
            textViewLevel = (TextView) itemView.findViewById(R.id.textViewLevel);
            textViewTimeStamps = (TextView) itemView.findViewById(R.id.textViewTimeStamp);
        }
    }
}