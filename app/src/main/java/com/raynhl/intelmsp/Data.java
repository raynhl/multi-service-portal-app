package com.raynhl.intelmsp;

/**
 * Created by raynhl on 1/25/17.
 */

public class Data {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    private int id;
    private String timeStamp;
    private String jsonData;

    public Data(int id, String timeStamp, String jsonData) {
        this.id = id;
        this.timeStamp = timeStamp;
        this.jsonData = jsonData;
    }
}
