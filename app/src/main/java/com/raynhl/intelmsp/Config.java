package com.raynhl.intelmsp;

/**
 * Created by raynhl on 1/22/17.
 */

public class Config {
    public static String[]      facilityTypes;
    public static int[]         facilityIcons;
    public static int[]         facilitySubIcons;
    public static int[]         availables;
    public static String[]      buildingNames;
    public static String[]      levels;
    public static String[]      timeStamps;
    public static int[]         cardBackgroundColors;


    //public static final String GET_URL = "http://10.174.73.178:3000/getstatus";
    public static final String GET_URL = "http://10.174.74.72:5000/hello";

    // tags, aka keys to json object
    public static final String TAG_BUILDING_NAME = "building_name";
    public static final String TAG_FACILITY_TYPE = "facility_type";
    public static final String TAG_AVAILABLE     = "available";
    public static final String TAG_TOTAL         = "total";
    public static final String TAG_LEVEL         = "level";
    public static final String TAG_GENDER        = "gender";
    public static final String TAG_MAINTENANCE   = "maintenance";
    public static final String TAG_JSON_ARRAY       = "results";

    public Config (int i) {
        facilityTypes               = new String[i];
        facilityIcons               = new int[i];
        facilitySubIcons            = new int[i];
        availables                  = new int[i];
        buildingNames               = new String[i];
        levels                      = new String[i];
        timeStamps                  = new String[i];
        cardBackgroundColors        = new int[i];
    }
}
