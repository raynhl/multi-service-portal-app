package com.raynhl.intelmsp;

/**
 * Created by raynhl on 1/22/17.
 */

public class ListItem {

    private String buildingName;
    private String facilityType;
    private String timeStamp;
    private int facilityIcon;
    private int facilitySubIcon;
    private int available;
    private String level;
    private int cardBackgroundColor;

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getFacilityIcon() {
        return facilityIcon;
    }

    public void setFacilityIcon(int facilityIcon) {
        this.facilityIcon = facilityIcon;
    }

    public int getFacilitySubIcon() {
        return facilitySubIcon;
    }

    public void setFacilitySubIcon(int facilitySubIcon) {
        this.facilitySubIcon = facilitySubIcon;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getCardBackgroundColor() {
        return cardBackgroundColor;
    }

    public void setCardBackgroundColor(int cardBackgroundColor) {
        this.cardBackgroundColor = cardBackgroundColor;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getFacilityType() {
        return facilityType;
    }

    public void setFacilityType(String facilityType) {
        this.facilityType = facilityType;
    }
}
