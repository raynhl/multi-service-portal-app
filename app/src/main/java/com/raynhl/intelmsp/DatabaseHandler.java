package com.raynhl.intelmsp;

import android.content.ContentValues;
import android.content.Context;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by raynhl on 1/25/17.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "intel_tracker_db";
    private static final String TABLE_NAME = "AppData";
    private static final String KEY_ID = "id";
    private static final String KEY_TIMESTAMP = "timestamp";
    private static final String KEY_JSON_DATA = "json_data";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
                KEY_ID + " INT PRIMARY KEY, " + KEY_TIMESTAMP + " TEXT," + KEY_JSON_DATA + " TEXT" + ");";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    void addEntry(Data data) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, data.getId());
        values.put(KEY_TIMESTAMP, data.getTimeStamp());
        values.put(KEY_JSON_DATA, data.getJsonData());
        db.insert(TABLE_NAME, null, values);

        db.close();
    }

    public int updateData(Data data) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TIMESTAMP, data.getTimeStamp());
        values.put(KEY_JSON_DATA, data.getJsonData());

        int ret = db.update(TABLE_NAME, values, KEY_ID + " = ?",
                new String[]{String.valueOf(1)});

        return ret;
    }


    public Data getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectionArgs[] = {Integer.toString(id)};
        Cursor cursor = db.query(TABLE_NAME, null, KEY_ID + "=?", selectionArgs,
                                    null,null,null,null);

        if (cursor != null) {
            cursor.moveToFirst();

            try {
                Data data = new Data(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2));

                return data;
            } catch (CursorIndexOutOfBoundsException outOfBoundError) {
                return null;
            }

        }

        return null;
    }

    public void addOrUpdateData(Data data) {
        if (getData(1) == null) {
            addEntry(data);
        }

        else {
            int _status = updateData(data);
        }
    }
}
